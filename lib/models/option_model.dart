import 'package:flutter/material.dart';
// import 'package:weather_icons/weather_icons.dart';

class Option {
  Icon icon;
  String title;
  String subtitle;

  Option({this.icon, this.title, this.subtitle});
}

final options = [
  Option(
    icon: Icon(Icons.grass, size: 40.0),
    title: 'Weather Info',
    subtitle: 'Current Weather, Hourly & Daily Forecast.',
  ),
  Option(
    icon: Icon(Icons.water_damage, size: 40.0),
    title: 'Irrigation Info',
    subtitle: 'Crop Stage, Irrigation Forecast, Water Level and Advice.',
  ),
  Option(
    icon: Icon(Icons.eco_outlined, size: 40.0),
    title: 'Crop Health',
    subtitle: 'Satellite Imagery, Crop Health & Water Logging.',
  ),
  Option(
    icon: Icon(Icons.network_check, size: 40.0),
    title: 'Real Time Monitoring',
    subtitle: 'Soil Moisture, Soil Temp, Salinity, EC, TDS & History.',
  ),
  Option(
    icon: Icon(Icons.psychology, size: 40.0),
    title: 'Agronomist Consultancy',
    subtitle: 'Contact with Agronomist',
  ),
  Option(
    icon: Icon(Icons.emoji_nature, size: 40.0),
    title: 'Fertilizers',
    subtitle: 'NPK Values, Graphs & Fertilizer Requirements A/c to Stages.',
  ),
  // Option(
  //   icon: Icon(Icons.invert_colors, size: 40.0),
  //   title: 'Fertilizers',
  //   subtitle: 'NPK Values, Graphs & Fertilizer Requirements A/c to Stages.',
  // ),
  Option(
    icon: Icon(Icons.pest_control, size: 40.0),
    title: 'Pest',
    subtitle: 'Pest Control and Pest Predictions.',
  ),
  Option(
    icon: Icon(Icons.description, size: 40.0),
    title: 'About',
    subtitle: 'About Crop2x.',
  ),
];
